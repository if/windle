#!/usr/bin/env ruby

require 'sinatra/base'
require 'sqlite3'


# (LOCAL!!!) web server
class WebWindle < Sinatra::Base
  @@page_size = 16
  @@table_width = 4

  attr_accessor :page_size, :table_width

  def self.windle=(windle)
    @@windle = windle
  end

  set :public_folder, __dir__ + '/public'

  get '/' do
    page = params['page'] ? params['page'].to_i : 0
    cards = @@windle.list_cards.map{|pk| @@windle.get_card_pk(pk)}
    erb :index, locals: {:cards => cards, :tag => nil, :page => page,
                         :page_size => @@page_size, :table_width => @@table_width}
  end

  get '/tags' do
    tags = @@windle.list_tags
    erb :tags, locals: {:tags => tags}
  end
  
  get '/tag/:tag' do |tag|
    page = params['page'] ? params['page'].to_i : 0
    cards = @@windle.get_tagged(tag).map{|pk| @@windle.get_card_pk(pk)}
    erb :index, locals: {:cards => cards, :tag => tag, :page => page,
                         :page_size => @@page_size, :table_width => @@table_width}
  end
  
  get '/card/:id' do |id|
    if path = @@windle.path_by_card(id)
      path = path[0]
      card = @@windle.get_card path
      erb(:card, locals: {:card => card})
    else
      "*** No card with ID #{id}."
    end
  end

  post '/card/:id' do |id|
    if params['add-tag']
      @@windle.add_tag id, params['tag']
    elsif params['untag']
      @@windle.untag id, params['tag']
    end

    if params['add-link']
      @@windle.add_link id, params['link']
    elsif params['unlink']
      @@windle.unlink id, params['link']
    end

    redirect back
  end
  
  get '/img/:id' do |id|
    if (path = @@windle.path_by_card(id))
      path = path[0]
      path = File.exist?(path) ? path : 'public/not-found.png'
      File.read path
    end
  end
end


# class for facilitating interaction with the content database
class Windle
  def initialize(database_path)
    @db = SQLite3::Database.new database_path
  end

  def insert_card(path)
    @db.execute "INSERT INTO cards (path) SELECT ?\
 WHERE NOT EXISTS (SELECT 1 FROM cards WHERE path=(?))", [path, path]
  end

  def remove_card(path)
    pk = card_by_path path
    @db.execute "DELETE FROM cards WHERE pk=(?)", [pk]
    @db.execute "DELETE FROM tags WHERE card=(?)", [pk]
    @db.execute "DELETE FROM links WHERE card1=(?) OR card2=(?)", [pk, pk]
  end

  def update_card(path, new_path)
    pk = card_by_path path
    @db.execute "UPDATE cards SET path=(?) WHERE pk=(?)", [new_path, pk]
  end
  
  def add_tag(pk, tag)
    @db.execute "INSERT INTO tags (tag, card) SELECT ?, ?\
 WHERE NOT EXISTS (SELECT 1 FROM tags WHERE tag=(?) AND card=(?))",
                [tag, pk, tag, pk]
  end

  def add_link(pk1, pk2)
    @db.execute "INSERT INTO links (card1, card2) SELECT ?, ?\
 WHERE NOT EXISTS (SELECT 1 FROM links WHERE card1=(?) AND card2=(?))",
                [pk1, pk2, pk1, pk2]
  end

  # retrieve the row ID of the path
  def card_by_path(path)
    @db.execute("SELECT (pk) FROM cards WHERE path=(?)", [path])
  end

  def path_by_card(pk)
    @db.execute("SELECT (path) FROM cards WHERE pk=(?)", pk)[0]
  end

  def remove_tag(tag)
    @db.execute "DELETE FROM tags WHERE tag=(?)", [tag]
  end

  def untag(pk, tag)
    @db.execute "DELETE FROM tags WHERE tag=(?) AND card=(?)", [tag, pk]
  end

  def unlink(pk1, pk2)
    @db.execute "DELETE FROM links WHERE card1=(?) AND card2=(?)", [pk1, pk2]
    @db.execute "DELETE FROM links WHERE card1=(?) AND card2=(?)", [pk2, pk1]
  end

  def list_tags
    @db.execute("SELECT tag FROM tags").uniq.map{|t| t[0]}
  end

  def list_paths
    @db.execute("SELECT path FROM cards").map{|p| p[0]}
  end

  def list_cards
    @db.execute("SELECT pk FROM cards").map{|pk| pk[0]}
  end

  def get_tagged(tag)
    @db.execute("SELECT card FROM tags WHERE tag=(?)", [tag]).map{|pk| pk[0]}
  end
  
  def get_card(path)
    pk = card_by_path(path)[0][0]
    return nil if pk.nil?
    
    tags = @db.execute("SELECT tag FROM tags WHERE card=(?)", [pk]).map{|t| t[0]}
    links  = @db.execute("SELECT card1 FROM links WHERE card2=(?)", [pk]).map{|l| l[0]}
    links += @db.execute("SELECT card2 FROM links WHERE card1=(?)", [pk]).map{|l| l[0]}
    
    return {:path => path, :pk => pk, :tags => tags, :links => links}
  end

  def get_card_pk(pk)
    tags = @db.execute("SELECT tag FROM tags WHERE card=(?)", [pk]).map{|t| t[0]}
    links  = @db.execute("SELECT card1 FROM links WHERE card2=(?)", [pk]).map{|l| l[0]}
    links += @db.execute("SELECT card2 FROM links WHERE card1=(?)", [pk]).map{|l| l[0]}
    
    return {:pk => pk, :tags => tags, :links => links}
  end

  def get_tags(path)
    pk = card_by_path(path)[0][0]
    return nil if pk.nil?

    @db.execute("SELECT tag FROM tags WHERE card=(?)", [pk]).map{|t| t[0]}
  end
end



# contains methods for CLI actions; instantiated with a database
# connection (windle object)
class CLI
  def initialize(windle)
    @windle = windle
  end

  def register(path)
    path = File.expand_path path
    @windle.insert_card path
  end

  def unregister(path)
    path = File.expand_path path
    @windle.remove_card path
  end

  def check
    @windle.list_paths.each do |path|
      if File.exist? path
        puts "[OK] #{path}"
      else
        puts "[MISSING] #{path}"
        print "\trepair? ([y]es/[N]o/[r]emove) "
        s = STDIN.gets.chomp
        if s == "y"
          print "\tnew path: "
          new_path = STDIN.gets.chomp
          new_path = File.expand_path new_path
          if File.exist? new_path
            @windle.update_card path, new_path
            puts "\t#{path} -> #{new_path}"
          else
            puts "\tcan't find #{new_path}."
            puts "\tignoring. (didn't apply changes)"
          end
        elsif s == "r"
          unregister path
          puts "\tunregistered #{path}."
        else
          puts "\tignoring for now."
        end
      end
    end
  end

  def check_untagged
    @windle.list_paths.each do |path|
      if @windle.get_tags(path).empty?
        puts "Not tagged: #{path}"
      end
    end    
  end
end


if __FILE__ == $0
  HELP = "#{__FILE__} --add file | --remove file | --check | --check-tags | --run [port] | --help"
  db_path = ENV['WINDLE_DB'] || File.expand_path('~/.local/share/windle/default.db')
  w = Windle.new(db_path)
  cli = CLI.new(w)

  # not using a library for parsing args ¯\_(ツ)_/¯
  case ARGV[0]
  when '--add'
    path = ARGV[1]
    if path.nil?
      puts HELP
    else
      cli.register path
    end
  when '--remove'
    path = ARGV[1]
    if path.nil?
      puts HELP
    else
      cli.unregister path
    end
  when '--check'
    cli.check
  when '--check-tags'
    cli.check_untagged
  when '--run'
    if ARGV[1]
      WebWindle.port = ARGV[1].to_i
    end

    WebWindle.windle = w
    WebWindle.run!
  else
    puts HELP
  end
end
