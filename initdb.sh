#!/bin/sh
#
# Creates a windle database at the path specified by $1.

echo .quit | sqlite3 $1 -cmd "create table cards(pk INTEGER PRIMARY KEY, path VARCHAR(10))" && \
echo .quit | sqlite3 $1 -cmd "create table tags(pk INTEGER PRIMARY KEY, tag VARCHAR(10), card INTEGER)" && \
echo .quit | sqlite3 $1 -cmd "create table links(pk INTEGER PRIMARY KEY, card1 INTEGER, card2 INTEGER)"
