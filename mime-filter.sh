#!/bin/sh

# filter file based on their file type (currently set up to select images)
for f in $@
do
    if [ -n "$(file $f | grep "image")" ]
    then
        echo $f
        ./windle.rb --add $f
    fi
done
